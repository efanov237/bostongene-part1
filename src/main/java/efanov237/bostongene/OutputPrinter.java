package efanov237.bostongene;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

public class OutputPrinter implements Runnable {

    private final Memory memory;
    private final OutputStream out;
    private boolean isInterrupted = false;

    public OutputPrinter(Memory memory, OutputStream out) {
        this.memory = memory;
        this.out = out;
    }

    @Override
    public void run() {
        PrintWriter writer = new PrintWriter(new OutputStreamWriter(out, StandardCharsets.UTF_8));
        while (!isInterrupted) {
            if (!memory.isEmpty()) {
                String number = memory.findAndRemoveMin();
                writer.println(number);
                writer.flush();
            }
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                break;
            }
        }
        writer.close();
    }

    public void stop() {
        this.isInterrupted = true;
    }
}
