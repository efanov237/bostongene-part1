package efanov237.bostongene;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class Memory {

    private Map<String, Integer> memory = new HashMap<>();

    public synchronized void add(String words, Integer number) {
        memory.put(words, number);
    }

    public synchronized String findAndRemoveMin() {
        Map.Entry<String, Integer> minNumber = Collections.min(memory.entrySet(),
                Comparator.comparing(Map.Entry::getValue));
        String result = minNumber.getKey() + "(" + minNumber.getValue() + ")";
        memory.remove(minNumber.getKey());
        return result;
    }

    public boolean isEmpty() {
        return memory.isEmpty();
    }

    public int size() {
        return memory.size();
    }
}
