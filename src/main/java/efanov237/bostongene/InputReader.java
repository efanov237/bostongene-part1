package efanov237.bostongene;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class InputReader implements Runnable {

    private final Memory memory;
    private final InputStream in;

    public InputReader(Memory memory, InputStream in) {
        this.memory = memory;
        this.in = in;
    }

    @Override
    public void run() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
            String line = reader.readLine();
            while (!"exit".equals(line)) {
                try {
                    if (line == null || line.isEmpty()) throw new IOException("Empty line!");
                    memory.add(line, WordNumberConverter.getNum(line));
                } catch (IOException e) {
                    System.err.println(e.getMessage());
                }
                line = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
