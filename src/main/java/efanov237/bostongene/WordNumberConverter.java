package efanov237.bostongene;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class WordNumberConverter {

    private static final Map<String, Integer> DIGITS = new HashMap<String, Integer> () {{
        put("zero",  0);
        put("one",   1);
        put("two",   2);
        put("three", 3);
        put("four",  4);
        put("five",  5);
        put("six",   6);
        put("seven", 7);
        put("eight", 8);
        put("nine",  9);
        put("ten",      10);
        put("eleven",   11);
        put("twelve",   12);
        put("thirteen", 13);
        put("fourteen", 14);
        put("fifteen",  15);
        put("sixteen",  16);
        put("seventeen",17);
        put("eighteen", 18);
        put("nineteen", 19);
        put("twenty",   20);
        put("thirty",   30);
        put("forty",    40);
        put("fifty",    50);
        put("sixty",    60);
        put("seventy",  70);
        put("eighty",   80);
        put("ninety",   90);
    }};

    public static int getNum(String input) throws IOException {
        String[] a = input.split(" ");
        int n = 0;
        int g = 0;
        for(String str : a) {
            Integer x = DIGITS.get(str);
            if (x != null) {
                g = g + x;
            } else if ("hundred".equals(str)) {
                g = g * 100;
            } else {
                if ("thousand".equals(str)) {
                    n = n + g * 1000;
                    g = 0;
                } else {
                    throw new IOException("Unknown number: '" + str + "'!");
                }
            }
        }
        return n + g;
    }
}
