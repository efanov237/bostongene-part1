package efanov237.bostongene;

public class Application {

    public static void main(String[] args) throws InterruptedException {
        Memory memory = new Memory();

        InputReader reader = new InputReader(memory, System.in);
        Thread readerThread = new Thread(reader, "ReaderThread");
        readerThread.start();

        OutputPrinter printer = new OutputPrinter(memory, System.out);
        Thread printerThread = new Thread(printer, "PrinterThread");
        printerThread.start();

        readerThread.join();
        printer.stop();
    }
}