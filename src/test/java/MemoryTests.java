import efanov237.bostongene.Memory;
import org.junit.Assert;
import org.junit.Test;

public class MemoryTests {

    private Memory mem = new Memory();

    @Test
    public void addNewEntry() {
        mem.add("forty two", 42);
        Assert.assertFalse(mem.isEmpty());
    }

    @Test
    public void findMinimal() {
        mem.add("two", 2);
        mem.add("one", 1);
        mem.add("three", 3);
        Assert.assertEquals("one(1)", mem.findAndRemoveMin());
    }

    @Test
    public void removeMinimal() {
        mem.add("two", 2);
        mem.add("one", 1);
        mem.add("three", 3);
        mem.findAndRemoveMin();
        Assert.assertEquals(2, mem.size());
    }
}
