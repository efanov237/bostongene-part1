import efanov237.bostongene.Memory;
import efanov237.bostongene.OutputPrinter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.nio.charset.StandardCharsets;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class OutputPrinterTests {

    private Memory mockedMemory;

    private OutputPrinter printer;

    private BufferedReader outputStreamReader;


    @Before
    public void setUp() throws IOException {
        PipedInputStream pipeInput = new PipedInputStream();
        outputStreamReader = new BufferedReader(
                new InputStreamReader(pipeInput, StandardCharsets.UTF_8));
        BufferedOutputStream out = new BufferedOutputStream(
                new PipedOutputStream(pipeInput));
        mockedMemory = mock(Memory.class);
        printer = new OutputPrinter(mockedMemory, out);
    }

    @Test
    public void getAndPrintNumber() throws IOException {
        when(mockedMemory.findAndRemoveMin()).thenReturn("test");
        Thread th = new Thread(printer);
        th.start();
        Assert.assertEquals("test", outputStreamReader.readLine());
    }
}
