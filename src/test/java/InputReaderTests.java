import efanov237.bostongene.InputReader;
import efanov237.bostongene.Memory;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@Ignore //ToDo: fix nullptr
public class InputReaderTests {

    private Memory mockedMemory;

    private InputReader in;

    @Before
    public void setUp() {
        mockedMemory = mock(Memory.class);
        InputStream inputStream = new ByteArrayInputStream("forty two".getBytes(StandardCharsets.UTF_8));
        in = new InputReader(mockedMemory, inputStream);
    }


    @Test
    public void readLine() {
        Thread th = new Thread(in);
        th.start();
        verify(mockedMemory).add("forty two", 42);
    }
}
