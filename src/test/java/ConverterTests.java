import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

import static efanov237.bostongene.WordNumberConverter.getNum;

public class ConverterTests {

    @Test
    public void validNumbers() throws IOException {
        Assert.assertEquals(9879, getNum("nine thousand eight hundred seventy nine"));
        Assert.assertEquals(1234, getNum("one thousand two hundred thirty four"));
        Assert.assertEquals(27, getNum("twenty seven"));
    }

    @Test
    public void unknownNumberException() {
        try {
            getNum("invalid_number");
        } catch (IOException e) {
            Assert.assertEquals("Unknown number: 'invalid_number'!", e.getMessage());
        }
    }

    @Test
    public void emptyLineException() {
        try {
            getNum(" ");
        } catch (IOException e) {
            Assert.assertEquals("Empty line!", e.getMessage());
        }
    }
}
